package live.keane;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class PassengerTest {
    private Passenger passenger;

    @BeforeAll
    public static void init()
    {
        System.out.println("Starting tests");
    }

    @BeforeEach
    public void setUp()
    {
        passenger = new Passenger("", "", "", "", 0);
    }

    @Test
    public void testSetTitleSuccess()
    {
        passenger.setTitle("Mr");
        assertEquals("Mr", passenger.getTitle());

        passenger.setTitle("Mrs");
        assertEquals("Mrs", passenger.getTitle());

        passenger.setTitle("Ms");
        assertEquals("Ms", passenger.getTitle());
    }

    @Test
    public void testSetTitleFail()
    {
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
            passenger.setTitle("hello");
        });
        assertEquals("Invalid title", ex.getMessage());
    }

    @Test
    public void testSetNameSuccess()
    {
        passenger.setName("Jason Keane");
        assertEquals("Jason Keane", passenger.getName());
    }

    @Test
    public void testSetNameFail()
    {
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
            passenger.setName("a");
        });
        assertEquals("Invalid name", ex.getMessage());
    }

    @Test
    public void testSetIdSuccess()
    {
        passenger.setId("A1B2C3D4E5F6");
        assertEquals("A1B2C3D4E5F6", passenger.getId());
    }

    @Test
    public void testSetIdFail()
    {
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
            passenger.setId("a");
        });
        assertEquals("Invalid ID", ex.getMessage());
    }

    @Test
    public void testSetPhoneSuccess()
    {
        passenger.setPhone("0852706378");
        assertEquals("0852706378", passenger.getPhone());
    }

    @Test
    public void testSetPhoneFail()
    {
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
            passenger.setPhone("a");
        });
        assertEquals("Invalid phone", ex.getMessage());
    }

    @Test
    public void testSetAgeSuccess()
    {
        passenger.setAge(23);
        assertEquals(23, passenger.getAge());
    }

    @Test
    public void testSetAgeFail()
    {
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
            passenger.setAge(10);
        });
        assertEquals("Age too young", ex.getMessage());
    }
}